# PHP Image Gallery
WEBSITE
-------
http://titan.dcs.bbk.ac.uk/~wwalke02/w1fma/index.php

Description
-----------
Upload Jpeg images and it will be resized to a thumbnail and displayed in the image gallery. Click on a image 
to see the large version of it.

Installation
------------
To set permissions if you are using an Ftp client you can right click on a folder and look for file attributes.
The permissions on the "thumbnail" directory should be set to 777.
The permissions on the "imageLarge" directory should be set to 777.
The permissions on the "uploads" directory should be set to 777.

Configuration
-------------
Configuration settings for this application can be found in:
includes/config.inc.php
Please make the following changes:
Where -> mysql:host= "put your host details";
      -> dbname= "put your database name";
      -> $user = "put your username";
      -> $pass = "put your password";
	  
Create table in database using sql query below:
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL,
   PRIMARY KEY (`id`)

Languages
---------
The application supports the following languages:
English
German
To add an additional language copy and paste the lang array from english.php and save to a new language file.
Amend the value part of the array to the appropiate translation.
In header.php add the following code at the top of the page:
Where 'de' change to the name of the language file without the file extension, i.e :'spanish'.
if (isset($_GET['lang'])&& $_GET['lang'] === 'de'){
	$_SESSION['lang'] = $_GET['lang'];
}


