<?php
/**
 * Fma for Building Web Applications using MySQL and PHP (W1)
 * Author: Wayne Walker - wwalke02
 * Teacher: John Macnabb
 */ 
class Validate {
	
	private $error = array();
	
	//Constructor function
	public function __construct($dConnect){
		
	}
	public function validatePost($file, $lang, $title, $description) {
		//Check to see if a file is submitted
		if($file['error'] == UPLOAD_ERR_NO_FILE){
			$this->errors['files'] = $lang['Image Required'];
			return $this->errors;
		  //Check the file size
		} elseif($file['error'] == UPLOAD_ERR_INI_SIZE){
			$this->errors['files'] = $lang['Image Size'];
			return $this->errors;
		  //Check to see if title and description is submitted
		} elseif(empty($title) || empty($description)){
			$this->errors['files'] = $lang['Title_Des'];
			return $this->errors;
			//Check to see if file is uploaded
		} elseif(!is_uploaded_file($file['tmp_name'])) {
			$this->errors['files']= $lang['Upload error'];
			return $this->errors;
			//Check if image is a Jpeg
		} elseif($file['type'] != "image/jpeg") {
			$this->errors['files'] = $lang['Jpeg only'];
			return $this->errors;
		}
	}
	
	//Function to clean data
	public function cleanData($file) {
		$clean = trim($file);
		return htmlentities(strip_tags($clean));	
	}
	
	
}
?>