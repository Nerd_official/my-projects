<?php
/**
 * Fma for Building Web Applications using MySQL and PHP (W1)
 * Author: Wayne Walker - wwalke02
 * Teacher: John Macnabb
 */ 
class DataBase {
	public $dConnect;
	
	private $error = false;
	//Constuctor function
	public function __construct(){
		
	}
    //Function to insert query database
	public function insertQuery($dConnect, $sql, $values) {

		//param values
		$title = $values['title'];
	    $description = $values['description'];
		$path = $values['path'];
		$height = $values['height'];
		$width = $values['width'];
		
		try{
		//insert into database
		$stmt = $dConnect->prepare($sql);
		$stmt->bindParam(':title', $title);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':path', $path);
		$stmt->bindParam(':height', $height);
		$stmt->bindParam(':width', $width);
		$stmt->execute(); 
		} catch(PDOException $e){
			$this->error = true;
		}
		$stmt->closeCursor();	
		return $this->error;
	}
    //Function to select query database
	public function selectQuery($dConnect, $sql, $path) {
		//Select large image
		if($path == 'single'){
		    try{
				$stmt = $dConnect->prepare($sql);
				$stmt->execute(); 
				$result = $stmt->fetchAll();
				$stmt->closeCursor();
				return $result;	
			} catch(PDOException $e){
			    echo'query failed';
				exit;
		    }
				
		} else {
			//Select all images
			try{
				$stmt = $dConnect->prepare($sql);
				$stmt->bindParam(':path', $path);
				$stmt->execute(); 
				$result = $stmt->fetchAll();
				$stmt->closeCursor();
				return $result;
			} catch(PDOException $e){
			    echo'query failed';
				exit;
		    }	
		}
		
	}


}
?>