<?php
/**
 * Fma for Building Web Applications using MySQL and PHP (W1)
 * Author: Wayne Walker - wwalke02
 * Teacher: John Macnabb
 */ 
class Template {
	//Declare variables
	private $file;
	private $pass ="";
	private $pass2 ="";
	private $final;
	private $data;
	
	//Constuctor function
	public function __construct($path){
		//Check that file exists
		 if(is_file($path)) {
			 $this->file = file_get_contents($path);
			 $this->data = array();
		 } else {
			 echo'File does not exist';
			 exit();
		 }	 
		
	}
	//Function to set template data
	public function setData($key, $value, $key1, $value1, $key2, $value2) {
		
		$this->data[$key]= $value;
		if($key1 != null) {
		    $this->data[$key1]= $value1;
	    }
		if($key2 != null) {
		    $this->data[$key2]= $value2;
	    }
		//call display funtion
		$this->display();
		
	}
	//Function to display templates
	public function display() {
	
		foreach($this->data as $key => $value) {
			//Check for identifier then display the right content   
		    switch ($key) {
				 //Gallery page 
			case 'galleryLink' :
				$this->pass = str_replace('[+' . $key . '+]', $value, $this->file);
				break;
			case 'gallerySource' :
				$this->pass2 = str_replace('[+' . $key . '+]', $value, $this->pass);
				break;
			case 'galleryTitle' :
				$this->final = str_replace('[+' . $key . '+]', $value, $this->pass2);
				echo $this->final;
			    break;
			case 'Content' :
				$this->pass = str_replace('[+' . $key . '+]', $value, $this->file);
				echo $this->pass;
				break;
				//Error message
			case 'errors' :
				$this->pass = str_replace('[+' . $key . '+]', $value, $this->file);
				echo $this->pass;
				break;
				//Welcome message
			case 'welcome' :
				$this->pass = str_replace('[+' . $key . '+]', $value, $this->file);
				echo $this->pass;
				break;
				//Large image page
			case 'imageLarge' :
				$this->pass = str_replace('[+' . $key . '+]', $value, $this->file);
				break;
			case 'title' :
				$this->pass2 = str_replace('[+' . $key . '+]', $value, $this->pass);
				break;
			case 'description' :
				$this->final = str_replace('[+' . $key . '+]', $value, $this->pass2);
				echo $this->final;
			    break;
				//end div
			case 'end' :
				$this->pass = str_replace('[+' . $key . '+]', $value, $this->file);
				echo $this->pass;
				break;
			default :
				
			}


		}	
	}
}
?>