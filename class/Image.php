<?php
/**
 * Fma for Building Web Applications using MySQL and PHP (W1)
 * Author: Wayne Walker - wwalke02
 * Teacher: John Macnabb
 */ 
include 'class/Validate.php';
class Image
{
	
	private $dir;
	private $imgPath;
	private $errors = array();

	
	public function __construct($dir)
	{
		$this->dir = $dir;
		
	}
	
	//Function to process image and get image details
	public function galleryImage($file, $dataObject, $dConnect, $insert_sql, $title, $description, $lang) 
	{
		//Create an result array
		$result_array = array();
		
        //Create object to Validate.php
		$validateObject = new Validate($dConnect);
		$this->errors = $validateObject->validatePost($file, $lang, $title, $description);
		//Check if there are any errors in array
        if(!empty($this->errors)){
        	return $this->errors;
        }
		
		//Save thumb nail and large image and get array of image details	
		$result_array = $this->saveFile($file, $lang, $validateObject);
		
		//Insert image details into database if there are no errors
		if(count($result_array) > 1) {
	       $insertResultError = $dataObject->insertQuery($dConnect, $insert_sql, $result_array);
			//Check for errors from database insert
			if($insertResultError) {
				//Delete images from folders and display error message
				unlink($result_array['thumbFile']);
				unlink($result_array['largeFile']);
				$this->errors['files'] = $lang['Image Problem'];
			}
		} else {
			return $this->errors;
		}
		return $this->errors;	
	}
	
	// Save image to folders
	public function saveFile($type, $lang, $validateObject)
	{
		$files = $type;
		$name = $files['name'];	
		$tmp = $files['tmp_name'];	

		//Get the file extension		
	    $img_ext = $this->extension($name);
		//Rename image
		$name = $this->newName($img_ext);
		
		//Check that directory exists
		if(is_dir($this->dir)) {
			
			// Create path to save image
	        $savedPath = $this->dir.$name;
	        //Get the image filename
			$upfilename = basename($savedPath);
			
			//Get the image height and width
			$details = getimagesize($tmp);
			
			//Check if directory is writeable
			if(!is_writeable( __DIR__ . '/..'."/imageLarge") || !is_writeable(__DIR__ . '/..'."/thumbnail") || !is_writeable(__DIR__ . '/..'."/uploads")){
				echo'Directory not writeable';
				exit;
			}
		    //Set path to folders where images will be saved
			$largeFile =  __DIR__ . '/..'."/imageLarge/".$upfilename;
		    $thumbFile =  __DIR__ . '/..'."/thumbnail/".$upfilename;
			
			//Save temporary image to folder 
			if (move_uploaded_file($tmp, $savedPath)) {
			    //Resize image and save to thumbnail or largeImage folder
			    $newLargeImg = $this->resize($savedPath, $largeFile, 600, 600, 90, $details, $lang);
		        $newThumbImg = $this->resize($savedPath, $thumbFile, 150, 150, 90, $details, $lang);
				
				//Check if large image resize was successful
				if($newLargeImg[0]== true){
					//Sanitise and Save title and description, file path, width and height of image
					$details['path'] = $validateObject->cleanData($upfilename);
					$details['title'] = $validateObject->cleanData($_POST['title']);	
					$details['description'] = $validateObject->cleanData($_POST['description']);
					$details['height'] = $validateObject->cleanData($newLargeImg[3]);	
					$details['width'] = $validateObject->cleanData($newLargeImg[2]);
					
					//Save path of thumb nail file and large image file
					$details['thumbFile'] = $thumbFile;
					$details['largeFile'] = $largeFile;
					
					//upload success message
					$this->errors['files'] = $lang['Upload Success'];
					
					//Destroy temporary image
					unlink($savedPath);
					
					return $details;
				} else {
					return $newLargeImg;
				}
				
	        } else {
	        	//move file upload error		
	            return $this->errors['files'] = $lang['SaveFile Upload failed'];	
	        }	
		} else {
			//directory does not exist error
			return $this->errors['files'] = $lang['Directory'];
		}
	}

	//Function to get time stamp for new file name
	public function newName($newName) {
		return time() .'_'.rand(100,999) . '.' .$newName;
	}
	
	// Function to get file extension
	private function extension($file)
	{
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		return $ext;
	}
	
	//Function taken from hands on exercise to resize images
    public function resize($in_img_file, $out_file, $req_width, $req_height, $quality, $imgFile, $lang) 
	{

        // Get image width and height
	    $width = $imgFile[0];
		$height = $imgFile[1];
        // Open file according to file type
        $src = imagecreatefromjpeg($in_img_file);
        
        // Check if image is smaller (in both directions) than required image
        if ($width < $req_width and $height < $req_height) {
            // Use original image dimensions
            $new_width = $width;
            $new_height = $height;
        } else {
            // Test orientation of image and set new dimensions appropriately
	    // (makes sure largest dimension never exceeds the target thumb size)
            if ($width > $height) {
                // landscape
                $sf = $req_width / $width;
            } else {
                // portrait                 
		        $sf = $req_height / $height;
            }
            $new_width = round($width * $sf);
            $new_height = round($height * $sf);
        }

        // Create the new canvas ready for resampled image to be inserted into it
        $new = imagecreatetruecolor($new_width, $new_height);

        // Resample input image into newly created image
        imagecopyresampled($new, $src, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

        // Create output jpeg
        imagejpeg($new, $out_file, $quality);
        // Destroy any intermediate image files
        imagedestroy($src);
        imagedestroy($new);

        // Return an array of values, including the new width and height
        return array(true, "Resize successful", $new_width, $new_height);
    
	    // Return only the bool and an error message
        $this->error['files'] = $lang['Resize error'];
        return array(false);
	    
	}
	
}
?>