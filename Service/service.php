<?php
/**
 * Web Service for Building Web Applications using MySQL and PHP (W1)
 * Author: Wayne Walker - wwalke02
 * Teacher: John Macnabb
 */ 
//Include files
include dirname(__DIR__).'/includes/config.php';
include dirname(__DIR__).'/class/DataBase.php';
//Create database object
$dataObject = new DataBase();

if(isset($_GET['type'])){
$path = $_GET['type'];
$sql = "SELECT * FROM images where path =:path";

//Query database for information
$result = $dataObject->selectQuery($dConnect, $sql,$path);
if($result === false) {
	 echo "error";
} else {
     header('Content-type: application/json'); 
     $data = json_encode($result);
	 if(json_last_error() == JSON_ERROR_NONE){
     // No errors occurred
           echo $data;
     } else{
		   // Errors encountered
		   echo 'Something is wrong with JSON...';
		   echo 'CODE: ' . json_last_error();
     }		
}
}
?>