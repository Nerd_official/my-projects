<?php
/**
 * Access to Web Service for Building Web Applications using MySQL and PHP (W1)
 * Author: Wayne Walker - wwalke02
 * Teacher: John Macnabb
 */ 

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => 'http://titan.dcs.bbk.ac.uk/~wwalke02/w1fma/Service/service.php?type=1491866928_781.jpeg',
    CURLOPT_USERAGENT => 'Sample cURL Request',
	CURLOPT_HEADER, (false)
));
// Send the request & save response 
$return = curl_exec($curl);
$data = json_decode($return, true);

// Get the error codes and messages
if(curl_errno($curl)) {
    echo 'Code: ' . curl_errno($curl);
    echo 'Message: ' . curl_error($curl);
} elseif ($data == null){
	echo'No data to display';
} else {
    // Decode the response & process it
     foreach($data as $key => $id) {
        echo 'Title: ' .$id['title'].'<br>';
        echo 'Description: ' . $id['description'].'<br>';
        echo 'Filename: ' . $id['path'].'<br>';
        echo 'Height: ' . $id['height'].'<br>';
        echo 'Width: ' . $id['width'].'<br>';   
     }  
}

// Get array of info about the transfer
$info = curl_getinfo($curl);
// Close request to clear up some resources
curl_close($curl);


?>

