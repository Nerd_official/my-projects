<?php
/**
 * Fma for Building Web Applications using MySQL and PHP (W1)
 * Author: Wayne Walker - wwalke02
 * Teacher: John Macnabb
 */ 
      //Insert query for Image.php
	  $insert_sql = "INSERT INTO images (title, description, path, height, width) VALUES (:title, :description, :path, :height, :width)";
	  //Select query for View.php
	  $selectSql = "SELECT * FROM images where path = :path";
	  //Display query for gallery.php
	  $displaySql = "SELECT title, description, path FROM images";

?>
