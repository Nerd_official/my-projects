<!DOCTYPE html>
<?php

//Set language	
if (isset($_GET['lang'])&& $_GET['lang'] === 'english'){
	$_SESSION['lang'] = $_GET['lang'];
}
if (isset($_GET['lang'])&& $_GET['lang'] === 'de'){
	$_SESSION['lang'] = $_GET['lang'];
}
if (!isset($_SESSION['lang'])){
	$_SESSION['lang'] = 'english';
}
include 'language/' .$_SESSION['lang'] .'.php';

?>	
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="includes/fma_css.css">
<link href="https://fonts.googleapis.com/css?family=Bungee+Inline" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">


<title><?php echo $lang['index_title'];?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

</head>
<body>
	<div class="top">
		<div id ="top_form">
		<form enctype="multipart/form-data" action=" <?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="post">

		<label for="upload"><?php echo $lang['Upload Image'];?></label>
		<input type="file" name="img" id="upload">
		
		<label for="title"><?php echo $lang['Image Title'];?></label>
		<input type="text" name="title" id="title" maxlength="20" value="" />
		
		<label for="description"><?php echo $lang['Image Description'];?></label>
		<input type="text" name="description" id="description" value="" />
		
		<input type="submit" name="submit" value="<?php echo $lang['submit'];?>" />
	   </form>
	  </div>
	  <div class="header_title"><h2><?php echo $lang['index_title'];?></h2></div>
	</div>

