<?php
/**
 * 
 * Author: Wayne Walker - wwalke02
 * Teacher: John Macnabb
 */ 
//PDO
$dbInfo = 'mysql:host=xxxx;dbname=xxxx';
$user = 'xxxx';
$pass = 'xxxx';
try{
$dConnect = new PDO($dbInfo, $user, $pass);
$dConnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo'Connection failed: Please try again later';
	exit;
}
?>