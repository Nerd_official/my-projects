<?php
/**
 * Fma for Building Web Applications using MySQL and PHP (W1)
 * Author: Wayne Walker - wwalke02
 * Teacher: John Macnabb
 */ 
//Include files
include_once 'includes/includes.php';

//http://stackoverflow.com/questions/11094776/php-how-to-go-one-level-up-on-dirname-file#answer-11094813
$img = new Image(realpath(__DIR__ . '/..')."/uploads/");

//Get templates
$gallery = 'templates/galleryTemplate.php';
$index = 'templates/index_template.php';
$errors = 'templates/errorTemplate.php';
$welcome = 'templates/welcome_template.php';
$endDiv = 'templates/endDiv.php';

//Create objects
$galleryTemplate = new Template($gallery);
$dataObject = new DataBase();
$indexTemplate = new Template($index);
$errorTemplate = new Template($errors);
$welcomeTemplate = new Template($welcome);
$endTemplate = new Template($endDiv);

//Set template
$indexTemplate->setData('Content',"", null, null, null, null);

//Check image is submitted and process image
if (isset($_POST['submit'])){
     //Process image
	 $imageResults = $img->galleryImage($_FILES['img'], $dataObject, $dConnect, $insert_sql, $_POST['title'], $_POST['description'], $lang);
	 //Display errors if any
	 $errorTemplate->setData('errors',$imageResults['files'], null, null, null, null);
		
} else {
	 //Display welcome message
	 $welcomeTemplate->setData('welcome',$lang['welcome'], null, null, null, null);
}

//Query database for images to display
$result = $dataObject->selectQuery($dConnect, $displaySql,'single');

//Display image template
foreach ($result as $key => $row) { 
	$galleryTemplate->setData('galleryLink','index.php?page=view&type='. htmlentities($row['path']), 'gallerySource','thumbnail/'.htmlentities($row['path']), 'galleryTitle', htmlentities($row['title']));
}
//end div
$endTemplate->setData('end',null, null, null, null, null);
?>

