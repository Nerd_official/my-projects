<?php 
/**
 * Fma for Building Web Applications using MySQL and PHP (W1)
 * Author: Wayne Walker - wwalke02
 * Teacher: John Macnabb
 */ 
//Include functions
include 'includes/includes.php';

//Get templates
$viewImage = 'templates/viewTemplate.php';

//Create objects
$viewTemplate = new Template($viewImage);
$viewData = new DataBase();
$clean = htmlentities($_GET['type']);

//Query database
$result = $viewData->selectQuery($dConnect, $selectSql, $clean);
	
//Display image with title and description
foreach ($result as $key => $row) {     
	//Display template
	$viewTemplate->setData('imageLarge','imageLarge/'.htmlentities($row['path']), 'title', htmlentities($row['title']), 'description', htmlentities($row['description']));
}


?>



