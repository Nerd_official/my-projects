<?php
/**
 * Fma for Building Web Applications using MySQL and PHP (W1)
 * Author: Wayne Walker - wwalke02
 * Teacher: John Macnabb
 */ 
session_start();
// Include functions
include 'includes/includes.php';

//Get templates
$header = 'templates/header_template.php';
$footer = 'templates/footer_template.php';


//Create objects
$headerTemplate = new Template($header);
$footerTemplate = new Template($footer);


//Include header template
$headerTemplate->setData('header',include 'includes/header.php', null, null, null, null);

if (!isset($_GET['page'])) 
{
    $id = 'gallery'; // display image gallery
    require_once 'view/gallery.php';
} else {
    $id = $_GET['page']; // else requested page
    require_once 'view/view.php'; 
}	
//include footer template
$footerTemplate->setData('footer',include 'includes/footer.php', null, null, null, null);
?>

