<?php
//German language array
$lang = array(
    'footer' => 'Erstellen von Webanwendungen mit PHP und MYSQL 2017',
    'index_title' => 'Bildergalerie',
	'Upload Image' => 'Bild hochladen',
	'Image Title' => 'Bildtitel',
	'Image Description' => 'Bildbeschreibung',
	'Upload Success' => 'Bild erfolgreich hochgeladen',
	'Image Required' => 'Fehler: Bild erforderlich!',
	'Image Size' => 'Fehler: Bildgr��e ist gro�!',
	'Title_Des' => 'Fehler: Titel und Beschreibung erforderlich!',
	'Image Problem' => 'Es gab ein Problem beim Speichern von Bildern!',
	'Directory' => 'Verzeichnis existiert nicht',
	'Jpeg only' => 'Fehler - Bitte nur jpeg-Dateien hochladen!',
	'Upload error' => 'Beim Upload ist ein Fehler aufgetreten',
	'Resize error' => 'Problem beim �ffnen des Bildes',
	'SaveFile Upload failed' => 'Bild-Upload fehlgeschlagen',
	'submit' => 'Einreichen',
	'welcome' => 'Willkommen, bitte laden Sie ein Bild hoch'
	);
	
?>