<?php
//English language array
$lang = array(
    'footer' => 'Building Web Applications with PHP and MYSQL 2017',
    'index_title' => 'Image Gallery',
	'Upload Image' => 'Upload Image',
	'Image Title' => 'Image Title',
	'Image Description' => 'Image Description',
	'Upload Success' => 'Image successfully uploaded',
	'Image Required' => 'Error: Image Required!',
	'Image Size' => 'Error: Image size is to big!',
	'Title_Des' => 'Error: Title and Description Required!',
	'Image Problem' => 'There was a problem saving image!',
	'Directory' => 'Directory does not exist',
	'Jpeg only' => 'Error - Please upload jpeg files only!',
	'Upload error' => 'An error occured with the upload',
	'Resize error' => 'Problem opening image',
	'SaveFile Upload failed' => 'Image upload failed',
	'submit' => 'Submit',
	'welcome' => 'Welcome, please upload an image'
	);
	
?>